#!/bin/sh


#git rev-parse --short HEAD > VCS_REF
date -u +"%Y-%m-%d_%T%Z" > BUILD_DATE
git add BUILD_DATE
git commit -S \
  && git tag -s $(date +"%Y%m%d") \
  && git push origin --all \
  && git push origin --tags
